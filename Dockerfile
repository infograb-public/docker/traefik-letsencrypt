#FROM traefik:1.7-alpine
FROM traefik:v2.1.4

ADD startup.sh /startup.sh
CMD ["/startup.sh"]
